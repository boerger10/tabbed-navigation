﻿using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace TabbedNavigation
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new TabbedNavigationPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            Debug.WriteLine("On Start");
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            Debug.WriteLine("On Sleep");
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            Debug.WriteLine("On Resume");
        }
    }
}
