﻿using System.Diagnostics;
using Xamarin.Forms;

namespace TabbedNavigation
{
    public partial class TabbedNavigationPage : TabbedPage
    {
        public TabbedNavigationPage()
        {
            InitializeComponent();
        }

        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Navigation Tab Appearing");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Navigation Tab Disappearing");
        }
    }
}
