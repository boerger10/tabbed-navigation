﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace TabbedNavigation
{
    public partial class FirstTab : ContentPage
    {
        public FirstTab()
        {
            InitializeComponent();
        }

        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine("First Tab Appearing");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine("First Tab Disappearing");
        }
    }
}
