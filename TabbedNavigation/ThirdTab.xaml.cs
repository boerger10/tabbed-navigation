﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace TabbedNavigation
{
    public partial class ThirdTab : ContentPage
    {
        public ThirdTab()
        {
            InitializeComponent();
        }

        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Third Tab Appearing");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Third Tab Disappearing"); 
        }
    }
}
