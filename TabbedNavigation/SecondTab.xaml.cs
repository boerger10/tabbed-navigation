﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace TabbedNavigation
{
    public partial class SecondTab : ContentPage
    {
        public SecondTab()
        {
            InitializeComponent();
        }

        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Second Tab Appearing");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine("Second Tab Disappearing");
        }
    }
}
